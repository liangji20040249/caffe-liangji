import sys
sys.path.append('/home/liangji/workspace/caffe/bitbucket/caffe-liangji/scripts')
from pyproto import *




def one_step(bottom, top,step):

	s=''
	
	s+=get_conv_active(name='convr1'+step,bottom=bottom,top='convr1'+step,ksize=7,numoutput=8,pad=3,stride=2,paramname_w='convr1w',paramname_b='convr1b',active="ReLU")
	s+=get_pool(name='poolr1'+step,bottom='convr1'+step,top='poolr1'+step,pooltype='MAX',ksize=2,pad=0,stride=2)
	s+=get_conv_active(name='convr2'+step,bottom='poolr1'+step,top='convr2'+step,ksize=3,numoutput=16,pad=1,stride=1,paramname_w='convr2w',paramname_b='convr2b',active="ReLU")
	s+=get_pool(name='poolr2'+step,bottom='convr2'+step,top='poolr2'+step,pooltype='MAX',ksize=2,pad=0,stride=2)
	s+=get_conv_active(name='convr3'+step,bottom='poolr2'+step,top='convr3'+step,ksize=3,numoutput=32,pad=1,stride=1,paramname_w='convr3w',paramname_b='convr3b',active="ReLU")
	s+=get_conv_active(name='convr4'+step,bottom='convr3'+step,top='convr4'+step,ksize=3,numoutput=64,pad=1,stride=1,paramname_w='convr4w',paramname_b='convr4b',active="ReLU")
	s+=get_deconv_active(name='deconvr5'+step,bottom='convr4'+step,top='deconvr5'+step,ksize=4,numoutput=32,pad=1,stride=2,paramname_w='deconvr5w',paramname_b='deconvr5b',active="ReLU")
	s+=get_deconv_active(name='deconvr6'+step,bottom='deconvr5'+step,top='deconvr6'+step,ksize=4,numoutput=24,pad=1,stride=2,paramname_w='deconvr6w',paramname_b='deconvr6b',active="ReLU")
	s+=get_deconv(name=top,bottom='deconvr6'+step,top=top,ksize=4,numoutput=2,pad=1,stride=2,paramname_w='deconvr7w',paramname_b='deconvr7b')
	return s

def one_step2(bottom, top,step):

	s=''
	
	s+=get_conv_active(name='convr1'+step,bottom=bottom,top='convr1'+step,ksize=3,numoutput=8,pad=1,stride=2,paramname_w='convr1w',paramname_b='convr1b',active="ReLU")
	s+=get_conv_active(name='convr2'+step,bottom='convr1'+step,top='convr2'+step,ksize=3,numoutput=16,pad=1,stride=2,paramname_w='convr2w',paramname_b='convr2b',active="ReLU")
	s+=get_conv_active(name='convr3'+step,bottom='convr2'+step,top='convr3'+step,ksize=3,numoutput=32,pad=1,stride=2,paramname_w='convr3w',paramname_b='convr3b',active="ReLU")
	s+=get_conv_active(name='convr4'+step,bottom='convr3'+step,top='convr4'+step,ksize=3,numoutput=32,pad=1,stride=2,paramname_w='convr4w',paramname_b='convr4b',active="ReLU")

	s+=get_deconv_active(name='deconvr1'+step,bottom='convr4'+step,top='deconvr1'+step,ksize=4,numoutput=16,pad=1,stride=2,paramname_w='deconvr1w',paramname_b='deconvr1b',active="ReLU")
	s+=get_deconv_active(name='deconvr2'+step,bottom='deconvr1'+step,top='deconvr2'+step,ksize=4,numoutput=16,pad=1,stride=2,paramname_w='deconvr2w',paramname_b='deconvr2b',active="ReLU")
	s+=get_deconv_active(name='deconvr3'+step,bottom='deconvr2'+step,top='deconvr3'+step,ksize=4,numoutput=8,pad=1,stride=2,paramname_w='deconvr3w',paramname_b='deconvr3b',active="ReLU")
	s+=get_deconv(name=top,bottom='deconvr3'+step,top=top,ksize=4,numoutput=2,pad=1,stride=2,paramname_w='deconvr4w',paramname_b='deconvr4b')
	return s


def get_lfw(bottom,top,flag):
    	s=''
	s+=get_conv_active(name='conv1'+flag,bottom=bottom,top='conv1'+flag,ksize=5,numoutput=96,pad=0,stride=2,active="ReLU")
	s+=get_pool(name='pool1'+flag,bottom='conv1'+flag,top='pool1'+flag,pooltype='MAX',ksize=3,pad=0,stride=2)

	s+=get_conv_active(name='conv2'+flag,bottom='pool1'+flag,top='conv2'+flag,ksize=3,numoutput=96,pad=1,stride=1,active="ReLU")
	s+=get_pool(name='pool2'+flag,bottom='conv2'+flag,top='pool2'+flag,pooltype='MAX',ksize=3,pad=0,stride=2)

	s+=get_conv_active(name='conv3'+flag,bottom='pool2'+flag,top='conv3'+flag,ksize=3,numoutput=128,pad=1,stride=1,active="ReLU")
	s+=get_conv_active(name='conv4'+flag,bottom='conv3'+flag,top='conv4'+flag,ksize=3,numoutput=192,pad=1,stride=1,active="ReLU")
	s+=get_conv_active(name='conv5'+flag,bottom='conv4'+flag,top='conv5'+flag,ksize=3,numoutput=192,pad=1,stride=1,active="ReLU")
	s+=get_pool(name='pool5'+flag,bottom='conv5'+flag,top='pool5'+flag,pooltype='MAX',ksize=3,pad=0,stride=2)
	
	s+=get_deconv_active(name='deconv6'+flag,bottom='pool5'+flag,top='deconv6'+flag,ksize=6,numoutput=128,pad=0,stride=2,active="ReLU")
	s+=get_deconv_active(name='deconv7'+flag,bottom='deconv6'+flag,top='deconv7'+flag,ksize=6,numoutput=64,pad=0,stride=2,active="ReLU")
	s+=get_deconv(name='deconv8'+flag,bottom='deconv7'+flag,top='top',ksize=6,numoutput=2,pad=0,stride=2)
	
	return s

def get_lfw_small(bottom,top,flag):
    	s=''
	s+=get_conv_active(name='conv1'+flag,bottom=bottom,top='conv1'+flag,ksize=5,numoutput=8,pad=0,stride=2,active="ReLU")
	s+=get_pool(name='pool1'+flag,bottom='conv1'+flag,top='pool1'+flag,pooltype='MAX',ksize=3,pad=0,stride=2)

	s+=get_conv_active(name='conv2'+flag,bottom='pool1'+flag,top='conv2'+flag,ksize=3,numoutput=16,pad=1,stride=1,active="ReLU")
	s+=get_pool(name='pool2'+flag,bottom='conv2'+flag,top='pool2'+flag,pooltype='MAX',ksize=3,pad=0,stride=2)

	s+=get_conv_active(name='conv3'+flag,bottom='pool2'+flag,top='conv3'+flag,ksize=3,numoutput=32,pad=1,stride=1,active="ReLU")
	s+=get_conv_active(name='conv4'+flag,bottom='conv3'+flag,top='conv4'+flag,ksize=3,numoutput=64,pad=1,stride=1,active="ReLU")
	s+=get_conv_active(name='conv5'+flag,bottom='conv4'+flag,top='conv5'+flag,ksize=3,numoutput=64,pad=1,stride=1,active="ReLU")
	s+=get_pool(name='pool5'+flag,bottom='conv5'+flag,top='pool5'+flag,pooltype='MAX',ksize=3,pad=0,stride=2)
	
	s+=get_deconv_active(name='deconv6'+flag,bottom='pool5'+flag,top='deconv6'+flag,ksize=6,numoutput=32,pad=0,stride=2,active="ReLU")
	s+=get_deconv_active(name='deconv7'+flag,bottom='deconv6'+flag,top='deconv7'+flag,ksize=6,numoutput=8,pad=0,stride=2,active="ReLU")
	s+=get_deconv(name='deconv8'+flag,bottom='deconv7'+flag,top='top',ksize=6,numoutput=2,pad=0,stride=2)
	
	return s


# s=''
# s += one_step2('cnnout', 'step1_output','_step1')
# s += get_concat(name='concatr1',bottom=['data','step1_output'],top ='concatr1')
# s += one_step2('concatr1', 'step2_output','_step2')
# s += get_concat(name='concatr2',bottom=['data','step2_output'],top ='concatr2')
# s += one_step2('concatr2', 'step3_output','_step3')


s=''
s+=get_lfw('data','output','_lfw')

f=open('deploy.prototxt','w')
f.write(s)
f.close()














