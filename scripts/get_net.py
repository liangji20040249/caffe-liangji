from pyproto import *




def one_step(bottom, top,step):

	s=''
	
	s+=get_conv_active(name='convr1'+step,bottom=bottom,top='convr1'+step,ksize=7,numoutput=8,pad=3,stride=2,paramname_w='convr1w',paramname_b='convr1b',active="ReLU")
	s+=get_pool(name='poolr1'+step,bottom='convr1'+step,top='poolr1'+step,pooltype='MAX',ksize=2,pad=0,stride=2)
	s+=get_conv_active(name='convr2'+step,bottom='poolr1'+step,top='convr2'+step,ksize=3,numoutput=16,pad=1,stride=1,paramname_w='convr2w',paramname_b='convr2b',active="ReLU")
	s+=get_pool(name='poolr2'+step,bottom='convr2'+step,top='poolr2'+step,pooltype='MAX',ksize=2,pad=0,stride=2)
	s+=get_conv_active(name='convr3'+step,bottom='poolr2'+step,top='convr3'+step,ksize=3,numoutput=32,pad=1,stride=1,paramname_w='convr3w',paramname_b='convr3b',active="ReLU")
	s+=get_conv_active(name='convr4'+step,bottom='convr3'+step,top='convr4'+step,ksize=3,numoutput=64,pad=1,stride=1,paramname_w='convr4w',paramname_b='convr4b',active="ReLU")
	s+=get_deconv_active(name='deconvr5'+step,bottom='convr4'+step,top='deconvr5'+step,ksize=4,numoutput=32,pad=1,stride=2,paramname_w='deconvr5w',paramname_b='deconvr5b',active="ReLU")
	s+=get_deconv_active(name='deconvr6'+step,bottom='deconvr5'+step,top='deconvr6'+step,ksize=4,numoutput=24,pad=1,stride=2,paramname_w='deconvr6w',paramname_b='deconvr6b',active="ReLU")
	s+=get_deconv(name=top,bottom='deconvr6'+step,top=top,ksize=4,numoutput=2,pad=1,stride=2,paramname_w='deconvr7w',paramname_b='deconvr7b')
	return s

def one_step2(bottom, top,step):

	s=''
	
	s+=get_conv_active(name='convr1'+step,bottom=bottom,top='convr1'+step,ksize=5,numoutput=8,pad=2,stride=2,paramname_w='convr1w',paramname_b='convr1b',active="ReLU")
	s+=get_conv_active(name='convr2'+step,bottom='convr1'+step,top='convr2'+step,ksize=5,numoutput=16,pad=2,stride=2,paramname_w='convr2w',paramname_b='convr2b',active="ReLU")
	s+=get_conv_active(name='convr3'+step,bottom='convr2'+step,top='convr3'+step,ksize=3,numoutput=32,pad=1,stride=2,paramname_w='convr3w',paramname_b='convr3b',active="ReLU")

	s+=get_deconv_active(name='deconvr5'+step,bottom='convr3'+step,top='deconvr5'+step,ksize=4,numoutput=16,pad=1,stride=2,paramname_w='deconvr5w',paramname_b='deconvr5b',active="ReLU")
	s+=get_deconv_active(name='deconvr6'+step,bottom='deconvr5'+step,top='deconvr6'+step,ksize=4,numoutput=8,pad=1,stride=2,paramname_w='deconvr6w',paramname_b='deconvr6b',active="ReLU")
	s+=get_deconv(name=top,bottom='deconvr6'+step,top=top,ksize=4,numoutput=2,pad=1,stride=2,paramname_w='deconvr7w',paramname_b='deconvr7b')
	return s


s=''
s += one_step2('cnnout', 'step1_output','_step1')
s += get_concat(name='concatr1',bottom=['data','step1_output'],top ='concatr1')
s += one_step2('concatr1', 'step2_output','_step2')
s += get_concat(name='concatr2',bottom=['data','step2_output'],top ='concatr2')
s += one_step2('concatr2', 'step3_output','_step3')




f=open('deploy.prototxt','w')
f.write(s)
f.close()














