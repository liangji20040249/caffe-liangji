import caffe
from caffe import layers as L
from caffe import params as P



def Convolution(bottom, num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term=False):
    if bias_term:
        conv = L.Convolution(bottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,
                            bias_term = 1,
                            param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)],
                            weight_filler=dict(type='gaussian', std=0.01),
                            bias_filler=dict(type='constant', value=0))
    else:
        conv = L.Convolution(bottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,
                            bias_term = 0,
                            param=[dict(lr_mult=1, decay_mult=1)],
                            weight_filler=dict(type='gaussian', std=0.01))
    return conv
def BatchNorm(bottom):
    bn = L.BatchNorm(bottom, use_global_stats=False, in_place=True)
    return bn 
def Scale(bottom):
    scale = L.Scale(bottom, scale_param=dict(bias_term=True), in_place=True)
    return scale
    
def conv_relu(bottom, num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term=False):
    conv = Convolution(bottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)
        
    relu = L.ReLU(conv, in_place=True)
    return conv, relu

def deconv_relu(bottom, num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term=False):
    deconv = Convolution(bottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)
        
    relu = L.ReLU(deconv, in_place=True)
    return deconv, relu


def conv_bn_scale_relu(bottom, num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term=False):
    conv = Convolution(bottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)
    conv_bn = BatchNorm(conv)   
    conv_scale = Scale(conv_bn)
    #conv_bn = L.BatchNorm(conv, use_global_stats=False, in_place=True)
    #conv_scale = L.Scale(conv, scale_param=dict(bias_term=True), in_place=True)
    conv_relu = L.ReLU(conv, in_place=True)

    return conv, conv_bn, conv_scale, conv_relu




def return_net_str(net,startlinenum=10):
    ss = str(net.to_proto())
    flag = '\n'
    ss = ss.split(flag)
    ss = flag.join(ss[startlinenum:])
    return ss
    
    
def get_conv(bottomname, stagename,num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term = False):
    n = caffe.NetSpec()
    n.dummybottom, n.label = L.SegmentData(segment_data_param=dict(batch_size=3,shuffle=1), ntop=2)

    netstr = 'n.(stage),n.relu_(stage) = convolution(n.dummybottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)'

    exec(netstr.replace('(stage)',stagename))

    return return_net_str(n).replace('dummybottom',bottomname)

def get_deconv(bottomname, stagename,num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term = False):
    n = caffe.NetSpec()
    n.dummybottom, n.label = L.SegmentData(segment_data_param=dict(batch_size=3,shuffle=1), ntop=2)

    netstr = 'n.(stage),n.relu_(stage) = convolution(n.dummybottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)'

    exec(netstr.replace('(stage)',stagename))
    
    netstr = return_net_str(n).replace('dummybottom',bottomname)
    netstr = netstr.replace('Convolution','Deconvolution')

    return netstr
    
def get_conv_relu(bottomname, stagename,num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term = False):
    n = caffe.NetSpec()
    n.dummybottom, n.label = L.SegmentData(segment_data_param=dict(batch_size=3,shuffle=1), ntop=2)

    netstr = 'n.(stage),n.relu_(stage) = conv_relu(n.dummybottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)'

    exec(netstr.replace('(stage)',stagename))

    return return_net_str(n).replace('dummybottom',bottomname)

def get_deconv_relu(bottomname, stagename,num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term = False):
    n = caffe.NetSpec()
    n.dummybottom, n.label = L.SegmentData(segment_data_param=dict(batch_size=3,shuffle=1), ntop=2)

    netstr = 'n.(stage),n.relu_(stage) = deconv_relu(n.dummybottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=0, group=group,bias_term=bias_term)'

    exec(netstr.replace('(stage)',stagename))

    netstr = return_net_str(n)
    netstr = netstr.replace('dummybottom',bottomname)
    netstr = netstr.replace('Convolution','Deconvolution')
    return netstr

def get_conv_bn_scale_relu(bottomname, stagename,num_output=64, kernel_size=3, stride=1, pad=1, group=1,bias_term = False):
    n = caffe.NetSpec()
    n.dummybottom, n.label = L.SegmentData(segment_data_param=dict(batch_size=3,shuffle=1), ntop=2)

    netstr = 'n.(stage),n.bn_(stage),n.scale_(stage),n.relu_(stage) = conv_bn_scale_relu(n.dummybottom, num_output=num_output, kernel_size=kernel_size, stride=stride, pad=pad, group=group,bias_term=bias_term)'

    exec(netstr.replace('(stage)',stagename))

    return return_net_str(n).replace('dummybottom',bottomname)


ss = ''
ss += get_conv_bn_scale_relu('data', 'conv1',num_output=64, kernel_size=3, stride=1, pad=0, group=1,bias_term = 0)
#ss += get_deconv_relu('conv1', 'conv2',num_output=64, kernel_size=3, stride=1, pad=0, group=1,bias_term = 0)
print ss

#n = caffe.NetSpec()
#n.data, n.label = L.SegmentData(segment_data_param=dict(batch_size=3,shuffle=1), ntop=2)
#n.conv,n.relu = deconv_relu(n.data, num_output=64, kernel_size=3, stride=1, pad=0, group=1)
#ss = str(n.to_proto())

#print  ss



