# imports
import json
import time
import pickle
import scipy.misc
import skimage.io
import caffe
import cv2

import numpy as np
import os.path as osp

from xml.dom import minidom
from random import shuffle
from threading import Thread
from PIL import Image

#from tools import SimpleTransformer


class ImageDataLayer(caffe.Layer):

    """
    This is a simple syncronous datalayer for training a multilabel model on
    PASCAL.
    """

    def setup(self, bottom, top):

        self.top_names = ['data', 'label']

        # === Read input parameters ===

        # params is a python dictionary with layer parameters.
        params = eval(self.param_str)

        # store input as class variables
        self.batch_size = params['batch_size']

        self.new_height = params['new_height']
        self.new_width = params['new_width']

        # Create a batch loader to load the images.
        self.batch_loader = BatchLoader(params)

        # === reshape tops ===
        # since we use a fixed input image size, we can shape the data layer
        # once. Else, we'd have to do it in the reshape call.
        top[0].reshape(
            self.batch_size, 3, self.new_height, self.new_width)
        # Note the 20 channels (because PASCAL has 20 classes.)
        top[1].reshape(self.batch_size)

    def forward(self, bottom, top):
        """
        Load data.
        """
        for itt in range(self.batch_size):
            # Use the batch loader to load the next image.
            im, label = self.batch_loader.load_next_image()

            # Add directly to the caffe data layer
            top[0].data[itt, ...] = im
            top[1].data[itt] = label

    def reshape(self, bottom, top):
        top[0].reshape(
            self.batch_size, 3, self.new_height, self.new_width)
        top[1].reshape(self.batch_size)

    def backward(self, top, propagate_down, bottom):
        """
        These layers does not back propagate
        """
        pass


class BatchLoader(object):

    """
    This class abstracts away the loading of images.
    Images can either be loaded singly, or in a batch. The latter is used for
    the asyncronous data layer to preload batches while other processing is
    performed.
    """

    def __init__(self, params):
        self.batch_size = params['batch_size']
        self.new_height = params['new_height']
        self.new_width = params['new_width']
        self.source = params['source']
        self.root_folder = params['root_folder']
        self.mean_values = params['mean_values']
        # get list of image indexes.
        #list_file = params['split'] + '.txt'
        self.indexlist = open(self.source).read().splitlines()
        self._cur = 0  # current image
	self.shuffle = float(params['shuffle'])
        # this class does some simple data-manipulations
        #self.transformer = SimpleTransformer()

        print "BatchLoader initialized with {} images".format(
            len(self.indexlist))

	if self.shuffle>0:
		print 'shuffle list'
		np.random.shuffle(self.indexlist)

    def load_next_image(self):
        """
        Load the next image in a batch.
        """
        # Did we finish an epoch?
        if self._cur == len(self.indexlist):
            self._cur = 0
	    if self.shuffle>0:
	      print 'shuffle list'
              np.random.shuffle(self.indexlist)

        # Load an image
        image_file_name = self.indexlist[self._cur].split()[0]  # Get the image index
        
	impath = osp.join(self.root_folder, image_file_name)
	im = cv2.imread(impath)
	im = cv2.resize(im,(self.new_width,self.new_height))



	#im = np.asarray(Image.open(
        #    osp.join(self.root_folder, image_file_name)))
        #im = scipy.misc.imresize(im, [self.new_height, self.new_width])  # resize
       
	# h w c
	#im=np.swapaxes(im,1,0) 
	#cv2.imwrite('test.png',im)
	#exit()
	#print im.shape
        #im = im - np.array(self.mean_values).reshape(1,1,3)
        im = im - float(self.mean_values)
	#do a simple horizontal flip as data augmentation
        #flip = np.random.choice(2)*2-1
        #im = im[:, ::flip, :]


	#swap h w c --> c w h
	im=np.swapaxes(im,0,2)
	#swap c w h --> c h w
	im=np.swapaxes(im,1,2)
	
	#print 'flip',flip
	#cv2.imwrite('test.png',im)
	#exit()
	
        # Load and prepare ground truth
        label = np.asarray(self.indexlist[self._cur].split()[1], dtype=np.float32) 

        self._cur += 1
        return im, label

