                                                                            
import numpy                                                                                                                  
import os                                                                                                                                             
import sys                                                                      
 


def getallfilename(imgdir,extnames):                                            
    allfilenames = []                                                           
    for dirs, dirnames, files in os.walk(imgdir):                               
        for f in files:                                                         
            ext = os.path.splitext(f)[-1]                                       
            if ext.lower() in extnames:                                         
                filename = os.path.abspath(os.path.join(dirs,f))                
                allfilenames.append(filename)                                   
    return allfilenames                                                         
                                                                                
def getallimgfilename(imgdir):                                                  
    return getallfilename(imgdir,['.jpg','.jpeg','.bmp','.png','.pgm'])         
def getalltxtfilename(imgdir):                                                  
    return getallfilename(imgdir,['.txt'])                                      
def createfolderifnotexist(folder):                                             
    if not os.path.exists(folder):                                              
        os.mkdir(folder)                   



list_dirs = os.walk('.') 
for root, dirs, files in list_dirs: 
	#for d in dirs: 
		#print os.path.join(root, d)   
		
	   
	for f in files: 
		if not '~' in f:
			continue
		temp = os.path.join(root, f) 
		rule = 'rm '+temp
		print rule
		os.system(rule)



